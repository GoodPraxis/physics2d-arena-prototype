use bevy::{
    prelude::*,
    sprite::{MaterialMesh2dBundle, Mesh2dHandle},
};
use bevy_rapier2d::prelude::*;

use crate::{
    input::{ControllerAssignment, InputDevice, KeyboardControlSection},
    levels::SpawnLocation,
    state::SceneState,
};
use collision::CollisionPlugin;
use health::{Health, HealthPlugin};
use invincibility::InvincibilityPlugin;
use movement::{MovementPlugin, MovementStateBundle, Speed};

mod collision;
pub mod health;
mod invincibility;
pub mod movement;

const PLAYER_HEALTH: i16 = 3;

const PLAYER_WIDTH: f32 = 50.0;
pub const PLAYER_HEIGHT: f32 = 100.0;
const PLAYER_ONE_COLOR: Color = Color::CRIMSON;
const PLAYER_TWO_COLOR: Color = Color::BLUE;
const INVINCIBILITY_OPACITY: f32 = 0.8;

pub const PLAYER_GROUND_VELOCITY: f32 = 750.0;
pub const PLAYER_GROUND_VELOCITY_DAMPING: f32 = 10.0;
pub const PLAYER_AIR_VELOCITY: f32 = 100.0;
pub const PLAYER_AIR_VELOCITY_DAMPING: f32 = 1.5;
pub const PLAYER_JUMP_VELOCITY: f32 = 500.0;
pub const PLAYER_DASH_VELOCITY: f32 = 1500.0;

/// Used to determine the winner of a match.
#[derive(Debug, Clone, Copy, Component, PartialEq, Eq)]
pub enum Slot {
    One,
    Two,
}

impl Slot {
    /// get the other slot. i.e. when called on `Slot::One` it'll return `Slot::Two`.
    pub fn other(&self) -> Self {
        match self {
            Slot::One => Slot::Two,
            Slot::Two => Slot::One,
        }
    }
}

#[derive(Component, Debug)]
pub struct Player;

#[derive(Component, Debug)]
struct ColorSet {
    default: Handle<ColorMaterial>,
    opague: Handle<ColorMaterial>,
}

pub struct PlayersPlugin;

impl Plugin for PlayersPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(SceneState::InGame), spawn_player)
            .add_systems(OnEnter(SceneState::MainMenu), despawn_player)
            .add_systems(
                OnTransition {
                    from: SceneState::GameOver,
                    to: SceneState::InGame,
                },
                despawn_player,
            )
            .add_plugins(MovementPlugin)
            .add_plugins(HealthPlugin)
            .add_plugins(CollisionPlugin)
            .add_plugins(InvincibilityPlugin);
    }
}

/// # Panics
/// If the world has fewer than two `SpawnLocation`s
fn spawn_player(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    spawn_query: Query<&SpawnLocation>,
) {
    let mut spawn_locations_iter = spawn_query.iter();

    let spawn_location_one = spawn_locations_iter
        .next()
        .expect("there should be at least two SpawnLocations");
    let spawn_location_two = spawn_locations_iter
        .next()
        .expect("there should be at least two SpawnLocations");

    // Spawn player one
    let mesh = Mesh2dHandle(meshes.add(Rectangle::new(PLAYER_WIDTH, PLAYER_HEIGHT)));
    let material = materials.add(PLAYER_ONE_COLOR);
    commands
        .spawn(ColorSet {
            default: material.clone(),
            opague: materials.add(PLAYER_ONE_COLOR.with_a(INVINCIBILITY_OPACITY)),
        })
        .insert(MaterialMesh2dBundle {
            mesh: mesh.clone(),
            material,
            transform: Transform {
                translation: spawn_location_one.position,
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::KinematicPositionBased)
        .insert(KinematicCharacterController {
            snap_to_ground: Some(CharacterLength::Absolute(0.5)),
            ..default()
        })
        .insert(Speed::default())
        .insert(Collider::cuboid(PLAYER_WIDTH / 2.0, PLAYER_HEIGHT / 2.0))
        .insert(MovementStateBundle::default())
        .insert(ControllerAssignment(InputDevice::Keyboard(
            KeyboardControlSection::Left,
        )))
        .insert(Health(PLAYER_HEALTH))
        .insert(Player)
        .insert(Slot::One);

    //Spawn player two
    let material = materials.add(PLAYER_TWO_COLOR);
    #[cfg(feature = "controller_support")]
    let controller_assignment = ControllerAssignment(InputDevice::Gamepad(0));
    #[cfg(not(feature = "controller_support"))]
    let controller_assignment =
        ControllerAssignment(InputDevice::Keyboard(KeyboardControlSection::Right));

    commands
        .spawn(ColorSet {
            default: material.clone(),
            opague: materials.add(PLAYER_TWO_COLOR.with_a(INVINCIBILITY_OPACITY)),
        })
        .insert(MaterialMesh2dBundle {
            mesh,
            material,
            transform: Transform {
                translation: spawn_location_two.position,
                ..default()
            },
            ..default()
        })
        .insert(RigidBody::KinematicPositionBased)
        .insert(KinematicCharacterController {
            snap_to_ground: Some(CharacterLength::Absolute(0.5)),
            ..default()
        })
        .insert(Speed::default())
        .insert(Collider::cuboid(PLAYER_WIDTH / 2.0, PLAYER_HEIGHT / 2.0))
        .insert(MovementStateBundle::default())
        .insert(controller_assignment)
        .insert(Health(PLAYER_HEALTH))
        .insert(Player)
        .insert(Slot::Two);
}

fn despawn_player(query: Query<Entity, With<Player>>, mut commands: Commands) {
    for entity in &query {
        commands.entity(entity).despawn_recursive()
    }
}
