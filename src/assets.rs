use bevy::prelude::*;

#[derive(Resource, Debug, Default)]
pub struct GameAssets {
    pub sfx: SfxAssets,
    pub fonts: FontsAssets,
    pub icons: IconsAssets,
}

const JUMP_SFX: &str = "sfx/jump.wav";
const DENY_SFX: &str = "sfx/deny.wav";
const DASH_SFX: &str = "sfx/dash.wav";
const HURT_SFX: &str = "sfx/hurt.wav";

#[derive(Default, Debug)]
pub struct SfxAssets {
    pub jump: Handle<AudioSource>,
    pub deny: Handle<AudioSource>,
    pub dash: Handle<AudioSource>,
    pub hurt: Handle<AudioSource>,
}

const DEFAULT_FONT: &str = "fonts/FiraSans-Bold.ttf";

#[derive(Default, Debug)]
pub struct FontsAssets {
    pub default: Handle<Font>,
}

const DUST_PARTICLE: &str = "icons/px.png";
const FULL_HEART_ICON: &str = "icons/full.png";
const EMPTY_HEART_ICON: &str = "icons/empty.png";

#[derive(Default, Debug)]
pub struct IconsAssets {
    pub dust: Handle<Image>,
    pub full_heart: Handle<Image>,
    pub empty_heart: Handle<Image>,
}

pub struct AssetLoaderPlugin;

impl Plugin for AssetLoaderPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<GameAssets>()
            .add_systems(Startup, load_assets);
    }
}

fn load_assets(mut assets: ResMut<GameAssets>, asset_server: Res<AssetServer>) {
    *assets = GameAssets {
        sfx: SfxAssets {
            jump: asset_server.load(JUMP_SFX),
            deny: asset_server.load(DENY_SFX),
            dash: asset_server.load(DASH_SFX),
            hurt: asset_server.load(HURT_SFX),
        },
        fonts: FontsAssets {
            default: asset_server.load(DEFAULT_FONT),
        },
        icons: IconsAssets {
            dust: asset_server.load(DUST_PARTICLE),
            full_heart: asset_server.load(FULL_HEART_ICON),
            empty_heart: asset_server.load(EMPTY_HEART_ICON),
        },
    };
}
