use bevy::{audio::PlaybackMode, prelude::*};

use crate::assets::GameAssets;

#[derive(Debug, Clone, Copy)]
pub enum Sfx {
    Jump,
    Deny,
    Dash,
    Hurt,
}
impl Sfx {
    fn get(&self, assets: &Res<GameAssets>) -> Handle<AudioSource> {
        match self {
            Sfx::Jump => assets.sfx.jump.clone(),
            Sfx::Deny => assets.sfx.deny.clone(),
            Sfx::Dash => assets.sfx.dash.clone(),
            Sfx::Hurt => assets.sfx.hurt.clone(),
        }
    }
}

#[derive(Event)]
pub struct SfxEvent(pub Sfx);

pub struct SfxPlugin;

impl Plugin for SfxPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<SfxEvent>()
            .add_systems(Update, handle_sfx_event);
    }
}

fn handle_sfx_event(
    mut commands: Commands,
    mut sfx_events: EventReader<SfxEvent>,
    assets: Res<GameAssets>,
) {
    for &SfxEvent(sfx) in sfx_events.read() {
        commands.spawn(AudioBundle {
            source: sfx.get(&assets),
            settings: PlaybackSettings {
                mode: PlaybackMode::Despawn,
                ..default()
            },
        });
    }
}
