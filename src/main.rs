use bevy::{asset::AssetMetaCheck, prelude::*, window::PresentMode};
use bevy_particle_systems::ParticleSystemPlugin;
use bevy_rapier2d::prelude::*;

use assets::AssetLoaderPlugin;
use camera::CameraPlugin;
use debug::DebugPlugin;
use dust::DustPlugin;
use input::InputPlugin;
use levels::LevelsPlugin;
use players::PlayersPlugin;
use schedule::SchedulePlugin;
use sounds::SfxPlugin;
use state::StatePlugin;
use ui::UiPlugin;

mod assets;
mod camera;
mod debug;
mod dust;
mod input;
mod levels;
mod players;
mod schedule;
mod sounds;
mod state;
mod ui;

#[cfg(not(target_arch = "wasm32"))]
const PRESENT_MODE: PresentMode = bevy::window::PresentMode::Immediate;
#[cfg(target_arch = "wasm32")]
const PRESENT_MODE: PresentMode = bevy::window::PresentMode::Fifo;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.526, 0.808, 0.922)))
        // Itch returns 403 instead of 404 for meta files
        // leading to loading issues, this prevents that behaviour:
        .insert_resource(AssetMetaCheck::Never)
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                present_mode: PRESENT_MODE,
                resizable: false,
                ..default()
            }),
            ..default()
        }))
        .add_plugins(AssetLoaderPlugin)
        .add_plugins(DebugPlugin)
        .add_plugins(ParticleSystemPlugin)
        .add_plugins(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugins(RapierDebugRenderPlugin::default())
        .add_plugins(SchedulePlugin)
        .add_plugins(StatePlugin)
        .add_plugins(CameraPlugin)
        .add_plugins(LevelsPlugin)
        .add_plugins(DustPlugin)
        .add_plugins(SfxPlugin)
        .add_plugins(InputPlugin)
        .add_plugins(PlayersPlugin)
        .add_plugins(UiPlugin)
        .run();
}
