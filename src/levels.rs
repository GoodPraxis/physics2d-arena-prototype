use bevy::prelude::*;

use arena::ArenaPlugin;

mod arena;

pub struct LevelsPlugin;

impl Plugin for LevelsPlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<LevelSelect>().add_plugins(ArenaPlugin);
    }
}

/// Marker Component for despawning levels
#[derive(Component)]
struct Level;

/// State decides which level gets loaded
#[derive(States, Debug, Clone, Copy, Default, Eq, PartialEq, Hash)]
pub enum LevelSelect {
    #[default]
    Arena,
}

impl LevelSelect {
    pub fn name(&self) -> &str {
        match *self {
            Self::Arena => "Arena",
        }
    }

    pub fn next(&self) -> Self {
        match *self {
            Self::Arena => Self::Arena,
        }
    }

    pub fn prev(&self) -> Self {
        match *self {
            Self::Arena => Self::Arena,
        }
    }
}

#[derive(Component, Deref)]
pub struct SpawnLocation {
    pub position: Vec3,
}

impl SpawnLocation {
    pub fn new(position: Vec3) -> Self {
        Self { position }
    }
}
