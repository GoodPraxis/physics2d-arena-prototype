use bevy::{app::AppExit, prelude::*};

use crate::{assets::GameAssets, levels::LevelSelect, state::SceneState};

use self::level_select::LevelSelectUiPlugin;

use super::{
    clear_menu,
    styles::{button_style, menu_container_style, text_style, title_text_style, BUTTON_COLOR},
    ButtonQueryFilter,
};
use level_select::draw_level_select;

mod level_select;

#[derive(Component)]
struct MainMenu;

#[derive(Component)]
struct PlayButton;

#[derive(Component)]
struct QuitButton;

pub struct MainMenuPlugin;

impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(SceneState::MainMenu), draw_main_menu)
            .add_systems(OnExit(SceneState::MainMenu), clear_menu::<MainMenu>)
            .add_systems(
                Update,
                (
                    handle_play_button_interaction,
                    handle_quit_button_interaction,
                )
                    .run_if(in_state(SceneState::MainMenu)),
            )
            .add_plugins(LevelSelectUiPlugin);
    }
}

fn draw_main_menu(
    mut commands: Commands,
    assets: Res<GameAssets>,
    level_select: Res<State<LevelSelect>>,
) {
    commands
        .spawn((
            NodeBundle {
                style: menu_container_style(),
                ..default()
            },
            MainMenu,
        ))
        .with_children(|parent| {
            // Title
            parent.spawn(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "2D Arena\nPrototype".into(),
                        style: title_text_style(&assets),
                    }],
                    justify: JustifyText::Center,
                    ..default()
                },
                ..default()
            });

            // Play Button
            parent
                .spawn((
                    ButtonBundle {
                        style: button_style(),
                        background_color: BUTTON_COLOR.into(),
                        ..default()
                    },
                    PlayButton,
                ))
                .with_children(|parent| {
                    parent.spawn(TextBundle {
                        text: Text {
                            sections: vec![TextSection::new("Play", text_style(&assets))],
                            ..default()
                        },
                        ..default()
                    });
                });

            // Level Select
            draw_level_select(parent, &assets, level_select);

            // Quit Button
            #[cfg(not(target_arch = "wasm32"))]
            parent
                .spawn((
                    ButtonBundle {
                        style: button_style(),
                        background_color: BUTTON_COLOR.into(),
                        ..default()
                    },
                    QuitButton,
                ))
                .with_children(|parent| {
                    parent.spawn(TextBundle {
                        text: Text {
                            sections: vec![TextSection::new("Quit", text_style(&assets))],
                            ..default()
                        },
                        ..default()
                    });
                });
        });
}

fn handle_play_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<PlayButton>>,
    mut app_state: ResMut<NextState<SceneState>>,
) {
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            app_state.set(SceneState::InGame);
        }
    }
}

fn handle_quit_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<QuitButton>>,
    mut exit_event_writer: EventWriter<AppExit>,
) {
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            exit_event_writer.send(AppExit);
        }
    }
}
