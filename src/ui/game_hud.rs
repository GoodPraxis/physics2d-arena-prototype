use bevy::prelude::*;

use crate::{
    assets::GameAssets,
    players::{
        health::{DamageEvent, Health},
        Player, Slot,
    },
    state::SceneState,
};

use super::{
    clear_menu,
    styles::{game_hud_container_style, health_bar_container_style, HEALTH_BAR_BACKGROUND_COLOR},
};

#[derive(Component)]
struct GameHud;

#[derive(Component)]
struct HealthBar;

pub struct GameHudPlugin;

impl Plugin for GameHudPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(SceneState::InGame), draw_main_menu)
            .add_systems(OnExit(SceneState::GameOver), clear_menu::<GameHud>)
            .add_systems(Update, update_hud.run_if(in_state(SceneState::InGame)))
            .add_systems(Update, update_hud.run_if(in_state(SceneState::GameOver)));
    }
}

fn draw_main_menu(mut commands: Commands, assets: Res<GameAssets>) {
    commands
        .spawn((
            NodeBundle {
                style: game_hud_container_style(),
                ..default()
            },
            GameHud,
        ))
        .with_children(|parent| {
            // Player One Health Bar
            parent
                .spawn((
                    NodeBundle {
                        style: health_bar_container_style(),
                        background_color: HEALTH_BAR_BACKGROUND_COLOR.into(),
                        ..default()
                    },
                    HealthBar,
                    Slot::One,
                ))
                .with_children(|parent| {
                    parent.spawn((health_icon(&assets.icons.full_heart), Health(1)));
                    parent.spawn((health_icon(&assets.icons.full_heart), Health(2)));
                    parent.spawn((health_icon(&assets.icons.full_heart), Health(3)));
                });

            // Player Two Health Bar            parent
            parent
                .spawn((
                    NodeBundle {
                        style: health_bar_container_style(),
                        background_color: HEALTH_BAR_BACKGROUND_COLOR.into(),
                        ..default()
                    },
                    HealthBar,
                    Slot::Two,
                ))
                .with_children(|parent| {
                    parent.spawn((health_icon(&assets.icons.full_heart), Health(1)));
                    parent.spawn((health_icon(&assets.icons.full_heart), Health(2)));
                    parent.spawn((health_icon(&assets.icons.full_heart), Health(3)));
                });
        });
}

fn health_icon(icon: &Handle<Image>) -> ImageBundle {
    ImageBundle {
        image: icon.clone().into(),
        ..default()
    }
}

fn update_hud(
    mut damage_events: EventReader<DamageEvent>,
    players: Query<(&Slot, &Health), With<Player>>,
    health_bars: Query<(&Slot, &Children), With<HealthBar>>,
    mut health_icons: Query<(&Health, &mut UiImage)>,
    assets: Res<GameAssets>,
) {
    for event in damage_events.read() {
        let (player_slot, player_health) = players.get(event.target).unwrap();
        if let Some((_, children)) = health_bars
            .iter()
            .find(|(bar_slot, _)| &player_slot == bar_slot)
        {
            for &child in children.iter() {
                if let Ok((icon_health, mut image)) = health_icons.get_mut(child) {
                    if player_health >= icon_health {
                        image.texture = assets.icons.full_heart.clone();
                    } else {
                        image.texture = assets.icons.empty_heart.clone();
                    }
                }
            }
        }
    }
}
