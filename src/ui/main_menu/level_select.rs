use bevy::prelude::*;

use crate::{
    assets::GameAssets,
    levels::LevelSelect,
    state::SceneState,
    ui::{
        styles::{level_select_container_style, small_button_style, text_style, BUTTON_COLOR},
        ButtonQueryFilter,
    },
};

pub struct LevelSelectUiPlugin;

impl Plugin for LevelSelectUiPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (
                handle_prev_button_interaction,
                handle_next_button_interaction,
            )
                .run_if(in_state(SceneState::MainMenu)),
        );
    }
}

#[derive(Component)]
struct PrevButton;

#[derive(Component)]
struct NextButton;

#[derive(Component)]
struct LevelName;

pub fn draw_level_select(
    parent: &mut ChildBuilder,
    assets: &Res<GameAssets>,
    level_select: Res<State<LevelSelect>>,
) {
    parent
        .spawn(NodeBundle {
            style: level_select_container_style(),
            background_color: Color::GRAY.with_a(0.8).into(),
            ..default()
        })
        .with_children(|parent| {
            // left arrow
            parent
                .spawn((
                    ButtonBundle {
                        style: small_button_style(),
                        background_color: BUTTON_COLOR.into(),
                        ..default()
                    },
                    PrevButton,
                ))
                .with_children(|parent| {
                    parent.spawn(TextBundle {
                        text: Text {
                            sections: vec![TextSection::new("<", text_style(assets))],
                            ..default()
                        },
                        ..default()
                    });
                });

            // stage name
            parent.spawn((
                TextBundle {
                    text: Text {
                        sections: vec![TextSection::new(
                            level_select.get().name(),
                            text_style(assets),
                        )],
                        ..default()
                    },
                    ..default()
                },
                LevelName,
            ));
            // right arrow

            parent
                .spawn((
                    ButtonBundle {
                        style: small_button_style(),
                        background_color: BUTTON_COLOR.into(),
                        ..default()
                    },
                    NextButton,
                ))
                .with_children(|parent| {
                    parent.spawn(TextBundle {
                        text: Text {
                            sections: vec![TextSection::new(">", text_style(assets))],
                            ..default()
                        },
                        ..default()
                    });
                });
        });
}

fn handle_prev_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<PrevButton>>,
    current_level: Res<State<LevelSelect>>,
    mut next_level: ResMut<NextState<LevelSelect>>,
    mut level_name: Query<&mut Text, With<LevelName>>,
) {
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            next_level.set(current_level.prev());

            // Update level name
            if let Ok(mut text) = level_name.get_single_mut() {
                text.sections[0].value = current_level.prev().name().to_string();
            }
        }
    }
}

fn handle_next_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<NextButton>>,
    current_level: Res<State<LevelSelect>>,
    mut next_level: ResMut<NextState<LevelSelect>>,
    mut level_name: Query<&mut Text, With<LevelName>>,
) {
    // queue next level
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            next_level.set(current_level.next());

            // update level name
            if let Ok(mut text) = level_name.get_single_mut() {
                text.sections[0].value = current_level.next().name().to_string();
            }
        }
    }
}
