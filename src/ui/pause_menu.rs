use bevy::prelude::*;

use crate::{
    assets::GameAssets,
    state::{GameState, PauseEvent, SceneState},
};

use super::{
    clear_menu,
    styles::{button_style, menu_container_style, text_style, title_text_style, BUTTON_COLOR},
    ButtonQueryFilter,
};

#[derive(Component)]
struct PauseMenu;

#[derive(Component)]
struct ResumeButton;

#[derive(Component)]
struct QuitToMenuButton;

pub struct PauseMenuPlugin;

impl Plugin for PauseMenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(GameState::Paused), draw_pause_menu)
            .add_systems(OnExit(GameState::Paused), clear_menu::<PauseMenu>)
            .add_systems(
                Update,
                (
                    handle_resume_button_interaction,
                    handle_quit_to_menu_button_interaction,
                ),
            );
    }
}

fn draw_pause_menu(mut commands: Commands, assets: Res<GameAssets>) {
    commands
        .spawn((
            NodeBundle {
                style: menu_container_style(),
                ..default()
            },
            PauseMenu,
        ))
        .with_children(|parent| {
            // Title
            parent.spawn(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "Paused".into(),
                        style: title_text_style(&assets),
                    }],
                    justify: JustifyText::Center,
                    ..default()
                },
                ..default()
            });

            // Resume Button
            parent
                .spawn((
                    ButtonBundle {
                        style: button_style(),
                        background_color: BUTTON_COLOR.into(),
                        ..default()
                    },
                    ResumeButton,
                ))
                .with_children(|parent| {
                    parent.spawn(TextBundle {
                        text: Text {
                            sections: vec![TextSection::new("Resume", text_style(&assets))],
                            ..default()
                        },
                        ..default()
                    });
                });

            // Quit To Main Menu Button
            parent
                .spawn((
                    ButtonBundle {
                        style: button_style(),
                        background_color: BUTTON_COLOR.into(),
                        ..default()
                    },
                    QuitToMenuButton,
                ))
                .with_children(|parent| {
                    parent.spawn(TextBundle {
                        text: Text {
                            sections: vec![TextSection::new("Quit To Menu", text_style(&assets))],
                            ..default()
                        },
                        ..default()
                    });
                });
        });
}

fn handle_resume_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<ResumeButton>>,
    mut pause_event: EventWriter<PauseEvent>,
) {
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            pause_event.send(PauseEvent);
        }
    }
}

fn handle_quit_to_menu_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<QuitToMenuButton>>,
    mut app_state: ResMut<NextState<SceneState>>,
) {
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            app_state.set(SceneState::MainMenu)
        }
    }
}
