use bevy::prelude::*;

use crate::assets::GameAssets;

pub const BUTTON_COLOR: Color = Color::rgb(0.15, 0.15, 0.15);
pub const HOVERED_BUTTON_COLOR: Color = Color::rgb(0.25, 0.25, 0.25);
pub const PRESSED_BUTTON_COLOR: Color = Color::rgb(0.35, 0.35, 0.35);

pub const HEALTH_BAR_BACKGROUND_COLOR: Color = Color::rgba(0.35, 0.35, 0.35, 0.5);

// === Shared ===

pub fn menu_container_style() -> Style {
    Style {
        flex_direction: FlexDirection::Column,
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        row_gap: Val::Px(8.0),
        width: Val::Percent(100.0),
        height: Val::Percent(100.0),
        ..default()
    }
}

pub fn title_text_style(assets: &Res<GameAssets>) -> TextStyle {
    TextStyle {
        font: assets.fonts.default.clone(),
        font_size: 64.0,
        color: Color::BLACK,
    }
}

pub fn button_style() -> Style {
    Style {
        width: Val::Px(200.0),
        height: Val::Px(80.0),
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        ..default()
    }
}

pub fn small_button_style() -> Style {
    Style {
        width: Val::Px(40.0),
        height: Val::Px(40.0),
        ..button_style()
    }
}

pub fn text_style(assets: &Res<GameAssets>) -> TextStyle {
    TextStyle {
        font: assets.fonts.default.clone(),
        font_size: 32.0,
        color: Color::WHITE,
    }
}

// === Main Menu ===

pub fn level_select_container_style() -> Style {
    Style {
        flex_direction: FlexDirection::Row,
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        column_gap: Val::Px(20.0),
        ..default()
    }
}

// === Game HUD ===

pub fn game_hud_container_style() -> Style {
    Style {
        flex_direction: FlexDirection::Row,
        justify_content: JustifyContent::SpaceBetween,
        align_items: AlignItems::Start,
        width: Val::Percent(100.0),
        height: Val::Percent(100.0),
        ..default()
    }
}

pub fn health_bar_container_style() -> Style {
    Style {
        flex_direction: FlexDirection::Row,
        justify_content: JustifyContent::SpaceAround,
        align_items: AlignItems::Center,
        width: Val::Px(100.0),
        height: Val::Px(50.0),
        ..default()
    }
}
