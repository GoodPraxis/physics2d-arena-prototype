use bevy::prelude::*;

use crate::{
    assets::GameAssets,
    state::{GameOverEvent, SceneState},
};

use super::{
    styles::{button_style, menu_container_style, text_style, title_text_style, BUTTON_COLOR},
    ButtonQueryFilter,
};

#[derive(Component)]
struct GameOverMenu;

#[derive(Component)]
struct RestartButton;

#[derive(Component)]
struct QuitToMenuButton;

pub struct GameOverMenuPlugin;

impl Plugin for GameOverMenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(SceneState::GameOver), draw_game_over_menu)
            .add_systems(OnExit(SceneState::GameOver), clear_game_over_menu)
            .add_systems(
                Update,
                (
                    handle_restart_button_interaction,
                    handle_quit_to_menu_button_interaction,
                ),
            );
    }
}

fn draw_game_over_menu(
    mut commands: Commands,
    assets: Res<GameAssets>,
    mut game_over_event: EventReader<GameOverEvent>,
) {
    if let Some(GameOverEvent(winner)) = game_over_event.read().next() {
        commands
            .spawn((
                NodeBundle {
                    style: menu_container_style(),
                    ..default()
                },
                GameOverMenu,
            ))
            .with_children(|parent| {
                // Title
                parent.spawn(TextBundle {
                    text: Text {
                        sections: vec![TextSection {
                            value: format!("Player {:?} wins", winner),
                            style: title_text_style(&assets),
                        }],
                        justify: JustifyText::Center,
                        ..default()
                    },
                    ..default()
                });

                // Restart Button
                parent
                    .spawn((
                        ButtonBundle {
                            style: button_style(),
                            background_color: BUTTON_COLOR.into(),
                            ..default()
                        },
                        RestartButton,
                    ))
                    .with_children(|parent| {
                        parent.spawn(TextBundle {
                            text: Text {
                                sections: vec![TextSection::new("Restart", text_style(&assets))],
                                ..default()
                            },
                            ..default()
                        });
                    });

                // Quit To Main Menu Button
                parent
                    .spawn((
                        ButtonBundle {
                            style: button_style(),
                            background_color: BUTTON_COLOR.into(),
                            ..default()
                        },
                        QuitToMenuButton,
                    ))
                    .with_children(|parent| {
                        parent.spawn(TextBundle {
                            text: Text {
                                sections: vec![TextSection::new(
                                    "Quit To Menu",
                                    text_style(&assets),
                                )],
                                ..default()
                            },
                            ..default()
                        });
                    });
            });
    }
}

fn clear_game_over_menu(mut commands: Commands, query: Query<Entity, With<GameOverMenu>>) {
    if let Ok(menu) = query.get_single() {
        commands.entity(menu).despawn_recursive();
    }
}

fn handle_restart_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<RestartButton>>,
    mut game_state: ResMut<NextState<SceneState>>,
) {
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            game_state.set(SceneState::InGame);
        }
    }
}

fn handle_quit_to_menu_button_interaction(
    query: Query<&Interaction, ButtonQueryFilter<QuitToMenuButton>>,
    mut app_state: ResMut<NextState<SceneState>>,
) {
    if let Ok(interaction) = query.get_single() {
        if *interaction == Interaction::Pressed {
            app_state.set(SceneState::MainMenu)
        }
    }
}
