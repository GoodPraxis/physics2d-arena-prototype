use bevy::prelude::*;

#[cfg(debug_assertions)]
use crate::players::movement::MovementState;
#[cfg(debug_assertions)]
use bevy::ecs::{archetype::Archetypes, component::Components, entity::Entities};

pub struct DebugPlugin;

impl Plugin for DebugPlugin {
    #[cfg(debug_assertions)]
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, add_debug_print)
            .add_systems(PostUpdate, update_debug_print)
            .add_systems(Update, inspect);
    }
    #[cfg(not(debug_assertions))]
    fn build(&self, _app: &mut App) {
        ()
    }
}

#[derive(Debug, Component)]
pub struct DebugPrint;

#[cfg(debug_assertions)]
fn add_debug_print(mut commands: Commands) {
    commands.spawn((
        TextBundle::from_sections([
            TextSection::new(
                "Grounded: ",
                TextStyle {
                    font_size: 20.0,
                    color: Color::BLACK,
                    ..default()
                },
            ),
            TextSection::from_style({
                TextStyle {
                    font_size: 20.0,
                    color: Color::BLUE,
                    ..default()
                }
            }),
        ]),
        DebugPrint,
    ));
}

#[cfg(debug_assertions)]
fn update_debug_print(
    mut text_query: Query<&mut Text, With<DebugPrint>>,
    query: Query<&MovementState>,
) {
    let Ok(mut text) = text_query.get_single_mut() else {
        return;
    };

    text.sections[1].value = String::new();
    for result in &query {
        text.sections[1].value += format!("{:?} ", result).as_str();
    }
}

// From @msklywenn https://github.com/bevyengine/bevy/issues/1467#issuecomment-840634560
#[cfg(debug_assertions)]
fn inspect(
    keyboard: Res<ButtonInput<KeyCode>>,
    all_entities: Query<Entity>,
    entities: &Entities,
    archetypes: &Archetypes,
    components: &Components,
) {
    if keyboard.just_pressed(KeyCode::F1) {
        for entity in all_entities.iter() {
            println!("Entity: {:?}", entity);
            if let Some(entity_location) = entities.get(entity) {
                if let Some(archetype) = archetypes.get(entity_location.archetype_id) {
                    for component in archetype.components() {
                        if let Some(info) = components.get_info(component) {
                            println!("\tComponent: {}", info.name());
                        }
                    }
                }
            }
        }
    }
}
