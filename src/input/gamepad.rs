use super::{InputDevice, InputMap, InputMapping};
use bevy::prelude::*;

pub fn map_gamepads(
    mut input_map: ResMut<InputMap>,
    gamepads: Res<Gamepads>,
    axis: Res<Axis<GamepadAxis>>,
    buttons: Res<ButtonInput<GamepadButton>>,
) {
    for gamepad in gamepads.iter() {
        let x = axis
            .get(GamepadAxis {
                gamepad,
                axis_type: GamepadAxisType::LeftStickX,
            })
            .unwrap_or(0.0);

        let just_dashed = buttons.just_pressed(GamepadButton {
            gamepad,
            button_type: GamepadButtonType::LeftTrigger,
        }) || buttons.just_pressed(GamepadButton {
            gamepad,
            button_type: GamepadButtonType::RightTrigger,
        });

        let jump = buttons.pressed(GamepadButton {
            gamepad,
            button_type: GamepadButtonType::South,
        });

        let just_jumped = buttons.just_pressed(GamepadButton {
            gamepad,
            button_type: GamepadButtonType::South,
        });

        input_map.0.insert(
            InputDevice::Gamepad(gamepad.id),
            InputMapping::new(x, just_dashed, jump, just_jumped),
        );
    }
}
