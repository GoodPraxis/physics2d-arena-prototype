use super::{ControllerAssignment, InputDevice, InputMap, InputMapping};
use bevy::prelude::*;

#[derive(Debug, Default, Hash, PartialEq, Eq, Clone, Copy)]
pub enum KeyboardControlSection {
    #[default]
    Left,
    #[allow(dead_code)] // FIXME: only dead due to hardcoded controller support if enabled
    Right,
}

impl KeyboardControlSection {
    fn get_mut_direction_input_order<'a>(
        &self,
        order: &'a mut ResMut<DirectionalInputOrder>,
    ) -> &'a mut Vec<KeyCode> {
        match self {
            KeyboardControlSection::Left => &mut order.left_section,
            KeyboardControlSection::Right => &mut order.right_section,
        }
    }

    fn get_direction_input_order<'a>(
        &self,
        order: &'a Res<DirectionalInputOrder>,
    ) -> &'a Vec<KeyCode> {
        match self {
            KeyboardControlSection::Left => &order.left_section,
            KeyboardControlSection::Right => &order.right_section,
        }
    }

    fn get_key_map(&self) -> KeyboardKeycodes {
        match self {
            KeyboardControlSection::Left => KEYBOARD_LEFT,
            KeyboardControlSection::Right => KEYBOARD_RIGHT,
        }
    }
}

/// In order to keep track of the latest directional input, in a sense letting
/// the player "buffer" the previous directional input until the latest has been
/// released. Only used for Keyboard - we can't push the joystick in both directions at once
#[derive(Debug, Default, Resource)]
pub struct DirectionalInputOrder {
    pub left_section: Vec<KeyCode>,
    pub right_section: Vec<KeyCode>,
}

struct KeyboardKeycodes {
    left: KeyCode,
    right: KeyCode,
    jump: KeyCode,
    dash_left: KeyCode,
    dash_right: KeyCode,
}

const KEYBOARD_LEFT: KeyboardKeycodes = KeyboardKeycodes {
    left: KeyCode::KeyA,
    right: KeyCode::KeyD,
    jump: KeyCode::Space,
    dash_left: KeyCode::KeyQ,
    dash_right: KeyCode::KeyE,
};

const KEYBOARD_RIGHT: KeyboardKeycodes = KeyboardKeycodes {
    left: KeyCode::KeyJ,
    right: KeyCode::KeyL,
    jump: KeyCode::ShiftRight,
    dash_left: KeyCode::KeyU,
    dash_right: KeyCode::KeyO,
};

pub fn update_directional_input_order(
    mut order: ResMut<DirectionalInputOrder>,
    assigned_controllers: Query<&ControllerAssignment>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    for controller_assignment in &assigned_controllers {
        #[allow(irrefutable_let_patterns)] // Only irrefutable without controller support
        if let ControllerAssignment(InputDevice::Keyboard(section)) = controller_assignment {
            let keymap = section.get_key_map();

            let section_order = section.get_mut_direction_input_order(&mut order);
            *section_order = section_order
                .clone()
                .into_iter()
                .filter(|&button| !keyboard_input.just_released(button))
                .collect::<Vec<KeyCode>>();

            section_order.extend(
                keyboard_input
                    .get_just_pressed()
                    .filter(|&&key| key == keymap.left || key == keymap.right)
                    .cloned(),
            );
        }
    }
}

pub fn map_keyboard(
    mut input_map: ResMut<InputMap>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
    assigned_controllers: Query<&ControllerAssignment>,
    directional_input_order: Res<DirectionalInputOrder>,
) {
    for controller_assignment in &assigned_controllers {
        #[allow(irrefutable_let_patterns)] // Only irrefutable without controller support
        if let ControllerAssignment(InputDevice::Keyboard(section)) = controller_assignment {
            let keymap = section.get_key_map();

            let latest_direction: Option<&KeyCode> = section
                .get_direction_input_order(&directional_input_order)
                .last();
            let x = if latest_direction == Some(&keymap.right) {
                1.0
            } else if latest_direction == Some(&keymap.left) {
                -1.0
            } else {
                0.0
            };

            let jump = keyboard_input.pressed(keymap.jump);
            let just_jumped = keyboard_input.just_pressed(keymap.jump);
            let just_dashed = keyboard_input.just_pressed(keymap.dash_left)
                || keyboard_input.just_pressed(keymap.dash_right);

            input_map.0.insert(
                InputDevice::Keyboard(*section),
                InputMapping::new(x, just_dashed, jump, just_jumped),
            );
        }
    }
}

pub fn reset_last_direction_inputs(mut order: ResMut<DirectionalInputOrder>) {
    order.left_section.clear();
    order.right_section.clear();
}
