pub mod state;

use bevy::prelude::*;
use bevy_rapier2d::prelude::*;

use self::state::{tick_air_time, AirTime};

use super::{
    Player, PLAYER_AIR_VELOCITY, PLAYER_AIR_VELOCITY_DAMPING, PLAYER_DASH_VELOCITY,
    PLAYER_GROUND_VELOCITY, PLAYER_GROUND_VELOCITY_DAMPING, PLAYER_JUMP_VELOCITY,
};
use crate::{
    dust::DustEvent,
    input::{ControllerAssignment, InputMap},
    schedule::UpdatePhase,
    sounds::{Sfx, SfxEvent},
};
use state::{tick_dash_timers, DashCooldownTimer, DashTimer};
pub use state::{MovementState, MovementStateBundle};

const GRAVITY: f32 = -500.0;
const GROUNDED_GRAVITY: f32 = -1.0;

#[derive(Default, Debug, Component)]
pub struct Speed {
    pub linvel: Vec2,
}

pub struct MovementPlugin;

impl Plugin for MovementPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Update,
            (
                jump,
                horizontal_movement,
                tick_dash_timers,
                tick_air_time,
                update_state,
                dampen_velocity,
                dash,
                apply_velocity,
            )
                .chain()
                .in_set(UpdatePhase::Movement),
        );
    }
}

fn dampen_velocity(
    mut query: Query<
        (
            &mut Speed,
            &MovementState,
            &KinematicCharacterControllerOutput,
        ),
        With<Player>,
    >,
    time: Res<Time>,
) {
    for (mut player_velocity, movement_state, controller) in &mut query {
        // No dampening during dash
        if movement_state == &MovementState::Dashing {
            continue;
        }

        let new_vx = if controller.grounded {
            player_velocity.linvel.x / (1.0 + PLAYER_GROUND_VELOCITY_DAMPING * time.delta_seconds())
        } else {
            player_velocity.linvel.x / (1.0 + PLAYER_AIR_VELOCITY_DAMPING * time.delta_seconds())
        };

        player_velocity.linvel.x = new_vx;
    }
}

fn horizontal_movement(
    mut query: Query<
        (
            &mut Speed,
            &ControllerAssignment,
            &MovementState,
            &KinematicCharacterControllerOutput,
        ),
        With<Player>,
    >,
    input_map: Res<InputMap>,
) {
    for (mut player_velocity, controller_assignment, movement_state, kinematic_controller) in
        &mut query
    {
        let device = &controller_assignment.0;
        let Some(map) = input_map.0.get(device) else {
            continue;
        };

        if movement_state == &MovementState::Dashing {
            continue;
        }

        // When on the ground, we go full speed, in the air we dampen until we are below air speed, else we maintain speed
        player_velocity.linvel.x = if kinematic_controller.grounded {
            PLAYER_GROUND_VELOCITY * map.x
        } else if player_velocity.linvel.x.abs() < PLAYER_AIR_VELOCITY
            || map.x.signum() != player_velocity.linvel.x.signum()
        {
            PLAYER_AIR_VELOCITY * map.x
        } else {
            player_velocity.linvel.x
        };
    }
}

pub fn dash(
    mut query: Query<
        (
            &mut Speed,
            &mut MovementState,
            &mut DashTimer,
            &mut DashCooldownTimer,
            &ControllerAssignment,
        ),
        With<Player>,
    >,
    mut sfx_event: EventWriter<SfxEvent>,
    input_map: Res<InputMap>,
) {
    for (
        mut player_velocity,
        mut movement_state,
        mut dash_timer,
        mut dash_cooldown_timer,
        controller_assignment,
    ) in &mut query
    {
        let device = &controller_assignment.0;
        let Some(map) = input_map.0.get(device) else {
            continue;
        };

        // Can't dash when still in cooldown
        if !dash_cooldown_timer.finished() {
            if map.just_dashed {
                sfx_event.send(SfxEvent(Sfx::Deny));
            }
            continue;
        }

        // When we dash, we take the direction of x to dash into
        if map.just_dashed {
            // If no direction is pressed, dash upwards
            if map.x == 0.0 {
                // But we dash upwards at half the speed
                player_velocity.linvel = Vec2::new(0.0, PLAYER_DASH_VELOCITY / 2.0);
            } else {
                player_velocity.linvel = Vec2::new(PLAYER_DASH_VELOCITY * map.x.signum(), 0.0);
            }

            sfx_event.send(SfxEvent(Sfx::Dash));

            *movement_state = MovementState::Dashing;
            dash_timer.reset();
            dash_cooldown_timer.reset();
        }
    }
}

fn jump(
    mut query: Query<
        (
            &mut Speed,
            &ControllerAssignment,
            &MovementState,
            &KinematicCharacterControllerOutput,
        ),
        With<Player>,
    >,
    mut sfx_event: EventWriter<SfxEvent>,
    input_map: Res<InputMap>,
    time: Res<Time>,
) {
    for (mut player_velocity, controller_assignment, movement_state, controller) in &mut query {
        let device = &controller_assignment.0;
        let Some(map) = input_map.0.get(device) else {
            continue;
        };

        // Can't jump during dash
        if movement_state == &MovementState::Dashing {
            if map.just_jumped {
                sfx_event.send(SfxEvent(Sfx::Deny));
            }
            return;
        }

        if map.just_jumped {
            if !controller.grounded {
                sfx_event.send(SfxEvent(Sfx::Deny));
                return;
            }
            player_velocity.linvel.y = PLAYER_JUMP_VELOCITY;
            sfx_event.send(SfxEvent(Sfx::Jump));
        }

        // If we didn't just jump and are grounded, we don't need to apply gravity
        let new_vy = if !map.just_jumped && controller.grounded {
            GROUNDED_GRAVITY
        } else if player_velocity.linvel.y > 0.0 {
            player_velocity.linvel.y + (GRAVITY * time.delta_seconds())
        } else {
            // Celeste doubles gravity while falling (maybe)
            // they do have a thing hwere they make the gravity weaker at the peak of your jump too though
            // only if you're holding jump
            // https://twitter.com/MaddyThorson/status/1238338576934260738
            player_velocity.linvel.y + ((GRAVITY * 2.0) * time.delta_seconds())
        };

        player_velocity.linvel.y = new_vy;
    }
}

type UpdateStateQuery<'a> = (
    &'a mut MovementState,
    &'a DashTimer,
    &'a GlobalTransform,
    &'a mut AirTime,
    &'a Speed,
    &'a KinematicCharacterControllerOutput,
);
fn update_state(
    mut query: Query<UpdateStateQuery, With<Player>>,
    mut dust_event: EventWriter<DustEvent>,
) {
    for (mut movement_state, dash_timer, transform, mut air_time, player_velocity, controller) in
        &mut query
    {
        // When we're still in dash, we don't want to update
        if *movement_state == MovementState::Dashing && !dash_timer.finished() {
            return;
        }

        // When we're still, or close to being still, we're Idle
        if player_velocity.linvel.x.abs() <= 0.1 && player_velocity.linvel.y.abs() <= 0.1 {
            *movement_state = MovementState::Idle;
        } else if controller.grounded {
            // If we get into the Walking state from Jumping, we want to spawn dust particles
            if *movement_state == MovementState::Jumping && air_time.elapsed_secs() > 0.2 {
                dust_event.send(DustEvent(*transform));
            }
            air_time.reset();
            air_time.pause();
            *movement_state = MovementState::Walking;
        } else {
            air_time.unpause();
            *movement_state = MovementState::Jumping;
        }
    }
}
fn apply_velocity(mut query: Query<(&Speed, &mut KinematicCharacterController)>, time: Res<Time>) {
    for (velocity, mut controller) in &mut query {
        controller.translation = Some(velocity.linvel * time.delta_seconds())
    }
}
