use bevy::prelude::*;

use super::{Player, Slot};
use crate::{
    schedule::UpdatePhase,
    sounds::{Sfx, SfxEvent},
    state::GameOverEvent,
};

#[derive(Debug, Event)]
pub struct DamageEvent {
    // TODO: add a source to the entity to avoid duplicated events
    pub target: Entity,
    pub amount: i16,
}
pub struct HealthPlugin;

impl Plugin for HealthPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<DamageEvent>().add_systems(
            Update,
            (apply_damage, check_health)
                .chain()
                .in_set(UpdatePhase::EntityUpdates),
        );
    }
}

#[derive(Component, Deref, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Health(pub i16);

fn apply_damage(
    mut query: Query<&mut Health, With<Player>>,
    mut damage_events: EventReader<DamageEvent>,
    mut sfx_event: EventWriter<SfxEvent>,
) {
    for event in damage_events.read() {
        sfx_event.send(SfxEvent(Sfx::Hurt));
        if let Ok(mut target) = query.get_mut(event.target) {
            target.0 -= event.amount;
            break; // only one damage event per frame, potential fix for "invicibility doesn't get added fast enough" bug
        }
    }
    damage_events.clear();
}

fn check_health(
    query: Query<(&Health, &Slot), With<Player>>,
    mut game_over_events: EventWriter<GameOverEvent>,
) {
    for (health, slot) in &query {
        if health.0 <= 0 {
            game_over_events.send(GameOverEvent(slot.other()));
        }
    }
}
