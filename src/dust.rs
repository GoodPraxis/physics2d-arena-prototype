//! Particle system example
use bevy::{prelude::*, transform::systems::propagate_transforms};
use bevy_particle_systems::*;

use crate::{assets::GameAssets, players::PLAYER_HEIGHT};

#[derive(Event)]
pub struct DustEvent(pub GlobalTransform);

pub struct DustPlugin;

impl Plugin for DustPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<DustEvent>()
            .add_systems(Update, spawn_dust_particles.before(propagate_transforms))
            .add_systems(Update, start_particle_emitters.after(propagate_transforms));
    }
}

fn spawn_dust_particles(
    mut commands: Commands,
    mut dust_events: EventReader<DustEvent>,
    assets: Res<GameAssets>,
) {
    for &DustEvent(transform) in dust_events.read() {
        let translation = transform.translation();
        let transform = Transform::from_xyz(
            translation.x,
            translation.y - (PLAYER_HEIGHT / 2.0),
            translation.z,
        );
        commands.spawn(ParticleSystemBundle {
            transform,
            particle_system: ParticleSystem {
                spawn_rate_per_second: 500.0.into(),
                initial_speed: JitteredValue::jittered(100.0, -0.2..0.2),
                lifetime: JitteredValue::jittered(0.2, -0.1..0.1),
                emitter_shape: CircleSegment {
                    radius: 1.0.into(),
                    opening_angle: std::f32::consts::PI,
                    direction_angle: std::f32::consts::PI / 2.0,
                }
                .into(),
                color: Color::rgba(0.52, 0.26, 0.03, 0.8).into(),
                initial_scale: JitteredValue::jittered(5.0, -3.0..3.0),
                looping: false,
                despawn_on_finish: true,
                z_value_override: Some(1.0.into()),
                system_duration_seconds: 0.2,
                initial_rotation: (-90.0_f32).to_radians().into(),
                rotate_to_movement_direction: true,
                texture: ParticleTexture::Sprite(assets.icons.dust.clone()),
                ..default()
            },
            ..default()
        });
    }
}

fn start_particle_emitters(
    mut commands: Commands,
    query: Query<Entity, (With<ParticleSystem>, Without<Playing>)>,
) {
    for entity in &query {
        commands.entity(entity).insert(Playing);
    }
}
