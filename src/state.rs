use bevy::prelude::*;

use crate::players::Slot;

/// State that handles scenes, like Main Menu and InGame, not to be confused with `GameState`, which is a substate
/// of the InGame state
#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Hash, States)]
pub enum SceneState {
    #[default]
    MainMenu,
    InGame,
    GameOver,
}

/// Handles pausing while in the InGame state
#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Hash, States)]
pub enum GameState {
    #[default]
    Running,
    Paused,
}

#[derive(Debug, Event)]
pub struct GameOverEvent(pub Slot);

#[derive(Debug, Event)]
pub struct PauseEvent;

pub struct StatePlugin;

impl Plugin for StatePlugin {
    fn build(&self, app: &mut App) {
        app.init_state::<SceneState>()
            .init_state::<GameState>()
            .add_event::<GameOverEvent>()
            .add_event::<PauseEvent>()
            .add_systems(
                PostUpdate,
                (
                    handle_pause_event.run_if(in_state(SceneState::InGame)),
                    transition_to_in_game.run_if(in_state(SceneState::GameOver)),
                    handle_game_over_event.run_if(in_state(SceneState::InGame)),
                ),
            )
            .add_systems(OnExit(SceneState::InGame), reset_game_states);
    }
}

fn handle_pause_event(
    mut next_state: ResMut<NextState<GameState>>,
    state: Res<State<GameState>>,
    mut pause_events: EventReader<PauseEvent>,
) {
    if !pause_events.is_empty() {
        match state.get() {
            GameState::Running => next_state.set(GameState::Paused),
            GameState::Paused => next_state.set(GameState::Running),
        }
        pause_events.clear();
    }
}

fn handle_game_over_event(
    mut next_state: ResMut<NextState<SceneState>>,
    mut game_over_events: EventReader<GameOverEvent>,
) {
    if !game_over_events.is_empty() {
        next_state.set(SceneState::GameOver);
        game_over_events.clear();
    }
}

fn transition_to_in_game(
    mut next_state: ResMut<NextState<SceneState>>,
    mut next_game_state: ResMut<NextState<GameState>>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::Enter) {
        next_state.set(SceneState::InGame);
        next_game_state.set(GameState::Running);
    }
}

fn reset_game_states(mut game_state: ResMut<NextState<GameState>>) {
    // Close pause menu if it was open
    game_state.set(GameState::Running);
}
