## Dependencies

This application was developed on Rust 1.77 (and this is reflected in the `rust-toolchain.toml` file). Other version might work without warranty.

Further, these system level dependencies are required, install these (on Ubuntu) or their equivalent on your system:

```
sudo apt-get install -y libasound2-dev libudev-dev
```

When in doubt try to compile the project and see what your compiler complains about being missing.

## Running the Game

To run the game simply execute

```
cargo r
```

to run it in debug mode with debug plugins enabled, alternatively append a `-r` flag to run it in release mode. Otherwise you can run the build version from [GitLab](https://gitlab.com/GoodPraxis/physics2d-arena-prototype/-/pipelines?page=1&scope=finished&status=success) or from [itch](https://good-praxis.itch.io/2d-platformer-arena?secret=fpGyanwmR0tOBdIl5I52ubOgCQ)

## How to Play

### Player One
- A & D - move left and right respectively.
- Space to jump.
- Q & E - dash into the direction you're moving.

Dash without speed to dash upwards.

### Player Two
If a controller is connected:

- Left-Stick to move.
- South button to jump.
- Shoulder buttons to dash in the direction you're moving.

Otherwise:
- J & L - move left and right respectively.
- Right-Shift to jump.
- U & O - dash in the direction you're moving.

And similar to Player One: Dash without speed to dash upwards.

## CI
Check out the [pipeline config](.gitlab-ci.yml) or see them in action on [Gitlab](https://gitlab.com/GoodPraxis/physics2d-arena-prototype/-/pipelines)

There are two kinds of private runners, those with the `#buildimage` tag and the untagged ones.
They are separate because the `#buildimage` runners require elevated permissions.
Not using public runners allows us to use the runners local caches without configuring a shared cache storage.

![CI pipeline illustration](CI.png)
